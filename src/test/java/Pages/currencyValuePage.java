package Pages;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cucumber.listener.Reporter;

import jdk.internal.org.jline.utils.Log;

import org.apache.commons.validator.routines.BigDecimalValidator;
import org.apache.commons.validator.routines.CurrencyValidator;
import org.junit.Assert;

public class currencyValuePage {

	WebDriver driver;
	
	@FindBy(id = "lbl_val_1")
	public WebElement lblValOne;
	
	@FindBy(id = "lbl_val_2")
	public WebElement lblValTwo;
	
	@FindBy(id = "lbl_val_3")
	public WebElement lblValThree;
	
	@FindBy(id = "lbl_val_4")
	public WebElement lblValFour;
	
	@FindBy(id = "lbl_val_5")
	public WebElement lblValFive;
	
	@FindBy(id = "lbl_ttl_va")
	public WebElement lblValttl;
	
	@FindBy(id = "txt_val_1")
	public WebElement txtValone;
	
	@FindBy(id = "txt_val_2")
	public WebElement txtValTwo;
	
	@FindBy(id = "txt_val_3")
	public WebElement txtValThree;
	
	@FindBy(id = "txt_val_4")
	public WebElement txtValFour;
	
	@FindBy(id = "txt_val_5")
	public WebElement txtValFive;
	
	@FindBy(id = "txt_ttl_val")
	public WebElement txtValttl;
	
	public currencyValuePage(WebDriver driver) {
		this.driver=driver;
	PageFactory.initElements(driver, this);
	}
	
	
	
	public void validateRightCntofVal(int count) {
		try {
		List<WebElement> values= driver.findElements(By.xpath("//*[contains(@id,'txt_val')]"));
		Assert.assertTrue(values.size()==count);
		}catch (Exception e) {
			Assert.assertTrue("Unable to validateRightCntofVal due to exception : "+e.getClass().getSimpleName(),false);
		}
	}
	
	public String getValOneTxt() {
		
		return txtValone.getText();
		
	}
	
	public String getValTwoTxt() {
		return txtValTwo.getText();
		
	}
	public String getValThreTxt() {
		return txtValThree.getText();
		
	}
	public String getValFourTxt() {
		return txtValFour.getText();
		
	}
	public String getValFiveTxt() {
		return txtValFive.getText();
		
	}
	
	public String getValTTLTxt() {
		return txtValttl.getText();
		
	}
	
	public WebElement getLblValFive() {
		return txtValFive;
		
	}
	
	
	
	
	
	
}
