package definations;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.cucumber.listener.Reporter;

import Utilities.PageUtilities;
import Utilities.ReportingUtility;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;
public class Hooks extends PageUtilities{	

	
	
	public ThreadLocal<ExtentTest> parentTest = new ThreadLocal<ExtentTest>();
	public static ThreadLocal<ExtentTest> test = new ThreadLocal<ExtentTest>();
	
	
	@Before
	public void beforeScenario(Scenario scenario) {
		
		switch (globalPropertValue("OS")) {
		case "MAC":
			WebDriverManager.chromedriver().setup();
			DesiredCapabilities chromeCaps = DesiredCapabilities.chrome();
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("excludeSwitches", Arrays.asList("test-type"));
			options.addArguments("--always-authorize-plugins=true");
			options.addArguments("test-type");
			options.addArguments("--disable-extensions");
			options.addArguments("--dns-prefetch-disable");
			options.addArguments("--start-maximized");
			options.addArguments("--disable-infobars");
			options.addArguments("enable-features=NetworkServiceInProcess");
			options.addArguments("--no-sandbox");
			options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
			this.driver = new ChromeDriver(options);
			this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			break;
			
		case "Windows":
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/Drivers/chromedriver");
		this.driver = new ChromeDriver();
		break;
		
		}
	}
	
	@After
	public void AfterSteps() {
		this.driver.quit();
	}
}