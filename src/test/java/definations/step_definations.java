package definations;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.LocaleUtils;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.aventstack.extentreports.Status;
import com.cucumber.listener.Reporter;

import Pages.currencyValuePage;
import Utilities.PageUtilities;
import Utilities.ReportingUtility;
import cucumber.api.java.ContinueNextStepsFor;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class step_definations extends PageUtilities {

	public currencyValuePage currValuePage = new currencyValuePage(driver);
	Locale lcl = null;
	SoftAssertions sf = new SoftAssertions();

	@Given("^launch currency calculator page$")
	public void launch_currency_calculator_page() throws Throwable {
		driver.get(globalPropertValue("url"));
		lcl = Locale.US;
	}

	@When("^Currency calculator page loaded succesfully$")
	public void Currency_calculator_page_loaded_succesfully() throws Throwable {
		if (currValuePage.getLblValFive().isDisplayed()) {
			Assert.assertTrue(true);
		} else {
			Assert.assertTrue(false);
		}
	}

	@ContinueNextStepsFor({ AssertionError.class })
	@Then("^verify the right count of values appear on the screen (\\d+)$")
	public void verify_the_right_count_of_values_appear_on_the_screen(int arg1) throws Throwable {
		currValuePage.validateRightCntofVal(arg1);
	}

	@ContinueNextStepsFor({ AssertionError.class })
	@Then("^verify the values on the screen are greater than (\\d+)$")
	public void verify_the_values_on_the_screen_are_greater_than(int arg1) throws Throwable {

		validateValGreatrThanZero(returnAmountofCurrency(currValuePage.getValOneTxt(), lcl));
		validateValGreatrThanZero(returnAmountofCurrency(currValuePage.getValTwoTxt(), lcl));
		validateValGreatrThanZero(returnAmountofCurrency(currValuePage.getValThreTxt(), lcl));
		validateValGreatrThanZero(returnAmountofCurrency(currValuePage.getValFourTxt(), lcl));
		validateValGreatrThanZero(returnAmountofCurrency(currValuePage.getValFiveTxt(), lcl));
	}

	@ContinueNextStepsFor({ AssertionError.class })
	@Then("^verify the total balance is correct based on the values listed on the screen$")
	public void verify_the_total_balance_is_correct_based_on_the_values_listed_on_the_screen() throws Throwable {

		Float sum = Float.parseFloat(returnAmountofCurrency(currValuePage.getValOneTxt(), lcl));
		String one = returnAmountofCurrency(currValuePage.getValOneTxt(), lcl);
		String two = returnAmountofCurrency(currValuePage.getValTwoTxt(), lcl);
		String three = returnAmountofCurrency(currValuePage.getValThreTxt(), lcl);
		String four = returnAmountofCurrency(currValuePage.getValFourTxt(), lcl);
		String five = returnAmountofCurrency(currValuePage.getValFiveTxt(), lcl);

		Float onef = Float.parseFloat(one);
		Float twof = Float.parseFloat(two);
		Float threef = Float.parseFloat(three);
		Float fourf = Float.parseFloat(four);
		Float fivef = Float.parseFloat(five);

		validatetwoSumsmatches(sum, new Float[] { onef, twof, threef, fourf, fivef });
	}

	@ContinueNextStepsFor({ AssertionError.class })
	@Then("^verify the values are formatted as currencies$")
	public void verify_the_values_are_formatted_as_currencies() throws Throwable {

		validateIsCurrency(currValuePage.getValOneTxt(), lcl);
		validateIsCurrency(currValuePage.getValTwoTxt(), lcl);
		validateIsCurrency(currValuePage.getValThreTxt(), lcl);
		validateIsCurrency(currValuePage.getValFourTxt(), lcl);
		validateIsCurrency(currValuePage.getValFiveTxt(), lcl);
	}

	@ContinueNextStepsFor({ AssertionError.class })
	@Then("^verify the total balance matches the sum of the values$")
	public void verify_the_total_balance_matches_the_sum_of_the_values() throws Throwable {
		
		Float sum = Float.parseFloat(returnAmountofCurrency(currValuePage.getValOneTxt(), lcl));
		String one = returnAmountofCurrency(currValuePage.getValOneTxt(), lcl);
		String two = returnAmountofCurrency(currValuePage.getValTwoTxt(), lcl);
		String three = returnAmountofCurrency(currValuePage.getValThreTxt(), lcl);
		String four = returnAmountofCurrency(currValuePage.getValFourTxt(), lcl);
		String five = returnAmountofCurrency(currValuePage.getValFiveTxt(), lcl);

		Float onef = Float.parseFloat(one);
		Float twof = Float.parseFloat(two);
		Float threef = Float.parseFloat(three);
		Float fourf = Float.parseFloat(four);
		Float fivef = Float.parseFloat(five);

		validatetwoSumsmatches(sum, new Float[] { onef, twof, threef, fourf, fivef });
	}

}
