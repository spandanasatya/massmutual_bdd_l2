package runners;

import java.io.File;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.cucumber.listener.Reporter;

import Utilities.ReportingUtility;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
features = "src/test/java/features"
,glue= {"definations"},plugin = { "pretty", "html:target/htmlreports" ,"com.cucumber.listener.ExtentCucumberFormatter:Reports/report.html"}
)

public class testrunner extends ReportingUtility{

	
//    public static void writeExtentReport() {
//        Reporter.loadXMLConfig(new File("config.xml"));
//    
//    }
	
	
	

}