package Utilities;

import org.junit.Before;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ReportingUtility {

	public static ExtentHtmlReporter htmlReporter=new ExtentHtmlReporter(System.getProperty("user.dir") +"/Reports/report.html");;
	public static ExtentReports reports;
	public static ExtentTest parentExtentTest;
	public static ExtentTest childTest;
	
	

	public ReportingUtility() {
		
	}
	
	public synchronized ExtentTest addChildTest(ExtentTest objParentTest, String strName, String strDesc) {
		ExtentTest objTest = null;
		objTest = objParentTest.createNode(strName, strDesc);
		return objTest;
	}
	
	public synchronized ExtentTest addTest(String strName, String StrDesc) {
		ExtentTest objTest = null;
		objTest = reports.createTest(strName, StrDesc);
		return objTest;
	}
    
	
}
