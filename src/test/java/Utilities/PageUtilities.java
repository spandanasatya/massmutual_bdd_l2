package Utilities;

import java.io.File;
import java.math.BigDecimal;
import java.util.Locale;

import org.apache.commons.validator.routines.BigDecimalValidator;
import org.apache.commons.validator.routines.CurrencyValidator;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class PageUtilities {

	public static WebDriver driver;

	public void validateIsCurrency(String currency, Locale countrycurrencyCode) {
		try {
			BigDecimalValidator validator = CurrencyValidator.getInstance();
			BigDecimal amount = validator.validate(currency, countrycurrencyCode);
			Assert.assertTrue(!(amount == null));
		} catch (Exception e) {
			Assert.assertTrue("Unable to validateIsCurrency due to exception : " + e.getClass().getSimpleName(),
					false);
		}
	}
	
	public String returnAmountofCurrency(String currency, Locale countrycurrencyCode) {
		String value = null;
		try {
			BigDecimalValidator validator = CurrencyValidator.getInstance();
			BigDecimal amount = validator.validate(currency, countrycurrencyCode);
			value=amount.toString();
		} catch (Exception e) {
			Assert.assertTrue("Unable to returnAmountofCurrency due to exception : " + e.getClass().getSimpleName(),
					false);
		}
		return value;
	}
	

	public void validateValGreatrThanZero(String value) {
		try {
			Assert.assertTrue(Integer.parseInt(value) > 0);
		} catch (Exception e) {
			Assert.assertTrue("Unable to validateValGreatrThanZero due to exception : " + e.getClass().getSimpleName(),
					false);
		}
	}

	public void validatetwoSumsmatches(Float sum, Float[] cummilative) {
		try {
			Float addition = null;
			for (Float f : cummilative) {
				addition = addition + f;
			}
			Assert.assertTrue(sum == addition);
		} catch (Exception e) {
			Assert.assertTrue("Unable to validatetwoSumsmatches due to exception : " + e.getClass().getSimpleName(),
					false);
		}
	}

	public static String globalPropertValue(String Key) {
		String key_value=null;
		try {
		String global_Data_Path = System.getProperty("user.dir") + File.separator
				+ "src/test/resources/global.properties";
		PropertyFileReader pfr = new PropertyFileReader(global_Data_Path);
		 key_value = pfr.getproperty(Key);
		}catch (Exception e) {
			Assert.assertTrue("Unable to capture globalPropertValue due to exception : " + e.getClass().getSimpleName(),
					false);
		}

		return key_value;
	}
	public String removeDollerSign(String txt) {
		return txt.split("$")[0].trim();
	}
}
